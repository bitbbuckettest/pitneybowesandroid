package com.example.abc.pritneybowestaskandroid;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.abc.pritneybowestaskandroid.adapter.MainAdapter;
import com.example.abc.pritneybowestaskandroid.model.ProductModel;
import com.example.abc.pritneybowestaskandroid.util.AppController;
import com.mancj.materialsearchbar.MaterialSearchBar;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity{
    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.searchBar)
    MaterialSearchBar searchBar;
    @BindView(R.id.help)
    TextView help;
    MainAdapter mAdapter;
    ArrayList<ProductModel> products;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        products = new ArrayList<>();

        mAdapter = new MainAdapter(this,products);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
//        getFurniture();

        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {

            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                products.clear();
                mAdapter.notifyDataSetChanged();
                getFurniture(text.toString());
                searchBar.setSelected(false);
                if (help.getVisibility()== View.VISIBLE){
                    help.setVisibility(View.GONE);
                }
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });
    }

    private void getFurniture(String query) {
        String url = "https://pitney-bowes-task.herokuapp.com/get/"+query;

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();

        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("response", response.toString());
                        for(int i=0;i<response.length();i++){
                            ProductModel product = new ProductModel();
                            try {
                                product.setName(response.getJSONObject(i).getString("product_name"));
                                product.setPrice(response.getJSONObject(i).getString("product_price"));
                                product.setImageUrl(response.getJSONObject(i).getString("product_image"));
                                product.setLink(response.getJSONObject(i).getString("product_link"));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            products.add(product);
                        }
                        mAdapter.notifyDataSetChanged();

                        pDialog.hide();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("response", "Error: " + error.getMessage());
                Toast.makeText(MainActivity.this, "heroku server sleeps if not used, so thus time-out. please try again", Toast.LENGTH_SHORT).show();
                pDialog.hide();
            }
        });

// Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }
}
