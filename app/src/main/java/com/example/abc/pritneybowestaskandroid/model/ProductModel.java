package com.example.abc.pritneybowestaskandroid.model;

/**
 * Created by abc on 13-12-2017.
 */

public class ProductModel {
    String name;
    String price;
    String imageUrl;
    String link;

    public ProductModel(String name, String price, String imageUrl, String link) {
        this.name = name;
        this.price = price;
        this.imageUrl = imageUrl;
        this.link = link;
    }

    public ProductModel() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
